alias b     := build
alias c     := check
alias d     := deploy
alias dd    := deploy-dry
alias f     := format
alias t     := test
alias s     := switch
alias u     := update

host := `hostname`

# check config
check:
    nix flake check

# build host to ./result/
build +HOST=host:
    nixos-rebuild build --flake .#{{HOST}}

# deploy config remotely
deploy HOST:
    nixos-rebuild switch --flake .#{{HOST}} --target-host root@{{HOST}} --use-remote-sudo

# deploy config remotely (dry run)
deploy-dry HOST:
    nixos-rebuild dry-activate --flake .#{{HOST}} --target-host root@{{HOST}} --use-remote-sudo

# enable NixOS configuration (dry run)
dry +HOST=host:
    nixos-rebuild dry-activate --use-remote-sudo --flake .#{{HOST}}

format:
    nix fmt

# enable NixOS configuration
switch +HOST=host:
    nixos-rebuild switch --use-remote-sudo --flake .#{{HOST}}

# enable NixOS configuration (test)
test +HOST=host:
    nixos-rebuild test --use-remote-sudo --flake .#{{HOST}}

# update flake inputs
update:
    nix flake update --commit-lock-file
