{
  config,
  lib,
  ...
}: let
  cfg = config.services.postgresql-backup;
in {
  options.services.postgresql-backup = {
    enable = lib.mkEnableOption "Backup SQL databases";
  };

  config = lib.mkIf cfg.enable {
    services.postgresqlBackup = {
      enable = true;
      backupAll = true;
      location = "/var/backup/postgresql";
    };

    services.backup = {
      paths = [
        config.services.postgresqlBackup.location
      ];
      # No need to store previous backups thanks to `services.backup (restic)`
      exclude = [
        (config.services.postgresqlBackup.location + "/*.prev.sql.gz")
      ];
    };
  };
}
