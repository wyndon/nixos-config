{
  description = "wyndon's NixOS config";

  inputs = {
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hardware.url = "github:NixOS/nixos-hardware";
    nixpkgs.url = "nixpkgs/nixos-unstable";
    nixpkgs-small.url = "nixpkgs/nixos-unstable-small";
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
  };

  outputs = inputs:
    with inputs; let
      inherit (self) outputs;
      eachSupportedSystem = nixpkgs.lib.genAttrs [
        "aarch64-linux"
        "x86_64-linux"
        "aarch64-darwin"
        "x86_64-darwin"
      ];
      keys = import ./keys.nix;
      pkgs = nixpkgs.legacyPackages;
    in {
      checks = eachSupportedSystem (system: {
        pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            alejandra.enable = true;
            statix.enable = true;
          };
        };
      });

      packages = eachSupportedSystem (system: import ./pkgs pkgs.${system});

      formatter = eachSupportedSystem (system: pkgs.${system}.alejandra);

      overlays = import ./overlay {inherit inputs;};

      devShells = eachSupportedSystem (system: {
        default = pkgs.${system}.mkShell {
          inherit (self.checks.${system}.pre-commit-check) shellHook;
          name = "nixos";

          buildInputs = with pkgs.${system}; [
            agenix.packages.${system}.default
            just
            nixos-rebuild
            statix
          ];
        };
      });

      nixosConfigurations = {
        rust = nixpkgs-small.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = {
            inherit inputs outputs keys;
          };
          modules = [
            hardware.nixosModules.common-cpu-intel
            hardware.nixosModules.common-pc-ssd
            agenix.nixosModules.age
            ./hosts/rust
            ./modules
            {
              nixpkgs.config.allowUnfree = true;
            }
          ];
        };
      };
    };
}
