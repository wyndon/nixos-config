{
  config,
  lib,
  inputs,
  outputs,
  keys,
  pkgs,
  ...
}: {
  age.secrets = {
    "users/password-root-hash".file = ../../../secrets/users/root/password-hash.age;
    "users/password-wyndon-hash".file =
      ../../../secrets/users/wyndon/password-hash.age;
  };

  boot.kernelPackages = lib.mkDefault pkgs.linuxPackages_latest;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.enable = true;
  boot.tmp.useTmpfs = true;

  time.timeZone = "Europe/Paris";
  i18n.defaultLocale = "en_US.UTF-8";
  console.keyMap = "us";

  hardware.enableAllFirmware = true;

  networking.useDHCP = false;

  nixpkgs.overlays = [
    outputs.overlays.additions
    outputs.overlays.modifications
  ];

  environment.systemPackages = with pkgs; [
    btop
    gdu
    git
    helix
    pciutils
    lf
    mc
  ];

  # Run the garbage collector every now and then
  nix.gc = {
    automatic = true;
    dates = "daily";
    options = "--delete-older-than 15d";
  };

  # Add flakes support
  nix.extraOptions = ''
    experimental-features = nix-command flakes
    keep-outputs = true # prevents devShells from being garbage collected
  '';

  nix.nixPath = ["nixpkgs=${inputs.nixpkgs}"];

  nix.settings = {
    # Optimize files with identical contents using hard links
    auto-optimise-store = true;

    allowed-users = ["@wheel"];
    trusted-users = [
      "root"
      "@wheel"
    ];
  };

  programs.ssh.knownHosts = {
    openwrt = {
      hostNames = ["openwrt"];
      publicKey = "${keys.hosts.openwrt}";
    };
    rust = {
      hostNames = ["rust"];
      publicKey = "${keys.hosts.rust}";
    };
  };

  users.mutableUsers = false;
  users.users.root.hashedPasswordFile = config.age.secrets."users/password-root-hash".path;

  users.users.wyndon = {
    extraGroups = ["wheel"];
    isNormalUser = true;
    hashedPasswordFile = config.age.secrets."users/password-wyndon-hash".path;
    useDefaultShell = true;
    uid = 1000;
    openssh.authorizedKeys.keys = [keys.users.wyndon];
  };

  security.apparmor = {
    enable = true;
    packages = [pkgs.apparmor-profiles];
    killUnconfinedConfinables = true;
  };

  # Enable openssh to automatically generate ssh key for agenix to work
  services.openssh = {
    enable = true;
    startWhenNeeded = true;
    settings.UseDns = true;
  };

  zramSwap.enable = true;
  zramSwap.algorithm = "zstd";
}
