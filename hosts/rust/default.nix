{
  config,
  keys,
  pkgs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
    ../common/core
    ./services
  ];

  age.secrets = {
    "backup/repo-passphrase".file = ../../secrets/backup/repo-passphrase.age;
  };

  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  boot.initrd.supportedFilesystems = ["zfs"];
  boot.supportedFilesystems = ["zfs"];

  documentation.enable = false;

  environment.systemPackages = [pkgs.httm];

  networking.hostId = "b6f68dd2"; # needed for ZFS
  networking.hostName = "rust";
  networking.interfaces.enp3s0.useDHCP = true;

  services.backup = {
    enable = true;
    repository = "local:/mnt/data/backups/restic/${config.networking.hostName}";
    passwordFile = config.age.secrets."backup/repo-passphrase".path;
  };

  services.sanoid = {
    enable = true;
    templates = {
      default = {
        hourly = 36;
        daily = 30;
        monthly = 3;
        yearly = 1;
        autosnap = true;
        autoprune = true;
      };
    };
    datasets = {
      "zdata/backups" = {useTemplate = ["default"];};
      "zdata/media" = {useTemplate = ["default"];};
    };
  };

  services.zfs.autoScrub.enable = true;

  system.autoUpgrade = {
    enable = false;
    flake = "git+https://codeberg.org/wyndon/nixos-config";
    dates = "04:40";
    allowReboot = true;
  };

  users.defaultUserShell = pkgs.bash;

  users.users = {
    root.openssh.authorizedKeys.keys = [keys.users.wyndon];
    wyndon.extraGroups = ["media"];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?
}
