{
  config,
  pkgs,
  lib,
  ...
}: let
  soulseekPort = 6080;
  transmissionDownloadsDir = "/mnt/data/downloads/transmission";
  nicotineHome = "/var/lib/nicotine";
  nicotineIds = 400;
in {
  networking.nat = {
    enable = true;
    internalInterfaces = ["ve-+"];
    externalInterface = "enp3s0";
    extraCommands = ''
      iptables -t nat -A POSTROUTING -o enp3s0 -j MASQUERADE
      iptables -t nat -A nixos-nat-post -d ${config.containers.treasure-hunting.localAddress} -p tcp --dport ${builtins.toString soulseekPort} -j SNAT --to-source ${config.containers.treasure-hunting.hostAddress}
      iptables -t nat -A nixos-nat-post -d ${config.containers.treasure-hunting.localAddress} -p tcp --dport 9091 -j SNAT --to-source ${config.containers.treasure-hunting.hostAddress}
      iptables -t nat -A nixos-nat-post -d ${config.containers.treasure-hunting.localAddress} -p tcp --dport 51413 -j SNAT --to-source ${config.containers.treasure-hunting.hostAddress}
    '';
  };

  system.activationScripts.nicotine = ''
    install -d -m 700 -o ${builtins.toString nicotineIds} -g ${builtins.toString nicotineIds} ${nicotineHome}
  '';

  containers.treasure-hunting = {
    autoStart = true;
    enableTun = true;
    ephemeral = true;

    privateNetwork = true;
    hostAddress = "192.168.0.10";
    localAddress = "192.168.0.11";
    forwardPorts = [
      {
        containerPort = soulseekPort;
        hostPort = soulseekPort;
        protocol = "tcp";
      }
      {
        containerPort = 9091;
        hostPort = 9091;
        protocol = "tcp";
      }
      {
        containerPort = 51413;
        hostPort = 51413;
        protocol = "tcp";
      }
    ];

    bindMounts = {
      "/var/lib/transmission" = {
        hostPath = "/var/lib/transmission";
        isReadOnly = false;
      };

      "/var/lib/nicotine" = {
        hostPath = "/var/lib/nicotine";
        isReadOnly = false;
      };

      "${transmissionDownloadsDir}" = {
        hostPath = transmissionDownloadsDir;
        isReadOnly = false;
      };

      "/home/downloads" = {
        hostPath = "/mnt/data/downloads/soulseek";
        isReadOnly = false;
      };

      "/home/music" = {
        hostPath = "/mnt/data/media/music";
        isReadOnly = true;
      };
    };

    config = {
      networking.nameservers = [
        "127.0.0.1"
      ];

      services.dnscrypt-proxy2 = {
        enable = true;
        settings = {
          listen_addresses = ["127.0.0.1:53"];

          block_ipv6 = true;
          doh_servers = false;
          odoh_servers = false;
          require_nolog = false;
          require_nofilter = false;

          forwarding_rules = pkgs.writeText "forwarding_rules.txt" ''
            loki 127.3.2.1
            snode 127.3.2.1
            0.10.in-addr.arpa 127.3.2.1
          '';
        };
      };

      users.users.nicotine = {
        home = nicotineHome;
        isSystemUser = true;
        group = "nicotine";
        uid = nicotineIds;
      };

      users.groups.nicotine = {
        gid = nicotineIds;
      };

      services.lokinet = {
        enable = true;
        useLocally = true;
        settings = {
          network.exit-node = [
            "euroexit.loki"
          ];
        };
      };

      services.transmission = {
        enable = true;
        openPeerPorts = true;
        openRPCPort = true;
        settings = {
          download-dir = transmissionDownloadsDir;
          download-queue-enabled = false;
          encryption = 2;
          idle-seeding-limit-enabled = true;
          idle-seeding-limit = 1; # pause seeding torrents after X minutes
          incomplete-dir = "${transmissionDownloadsDir}/.incomplete";
          message-level = 1; # only error logs
          rpc-authentication-required = false;
          rpc-bind-address = "0.0.0.0";
          rpc-whitelist-enabled = true;
          rpc-whitelist = "127.0.0.1,192.168.0.*";
          rpc-host-whitelist-enabled = true;
          rpc-host-whitelist = "localhost,rust";
          watch-dir-enabled = false;

          # umask value in base 10: echo $(( 8#umaskvalue ))
          umask = 7; # 007
        };
      };

      systemd.services.transmission.serviceConfig.BindPaths = [
        transmissionDownloadsDir
      ];

      # FIXME: https://github.com/NixOS/nixpkgs/issues/258793
      systemd.services.transmission.serviceConfig = {
        RootDirectoryStartOnly = lib.mkForce false;
        RootDirectory = lib.mkForce "";
      };

      users.groups.media.gid = 993;

      services.nginx = {
        enable = true;
        config = ''
          events {}
          http {
            # required to handle favicon.svg
            include ${pkgs.nginx}/conf/mime.types;

            server {
              listen ${builtins.toString soulseekPort};
              server_name localhost;

              location / {
                proxy_set_header Accept-Encoding "";
                proxy_pass http://localhost:8080/;
                sub_filter_once off;
                sub_filter "<title>" "<dummy>";
                sub_filter "</title>" "</dummy><title>Nicotine+</title><link rel=\"icon\" href=\"/favicon.svg\" type=\"image/svg+xml\">";
                sub_filter "broadway 2.0" "";
              }
            }
          }
        '';
      };

      networking.firewall.allowedTCPPorts = [soulseekPort];

      systemd.services.nicotine = {
        enable = true;
        description = "Nicotine";
        after = ["lokinet.service"];
        wants = ["lokinet.service"];
        wantedBy = ["multi-user.target"];

        environment = {
          BROADWAY_DISPLAY = ":0";
          GDK_BACKEND = "broadway";
          GTK_THEME = "Adwaita:dark";

          # makes icons working
          XDG_DATA_DIRS = "${pkgs.gnome.adwaita-icon-theme}/share:$XDG_DATA_DIRS";
        };

        preStart = ''
          ${pkgs.gtk3-x11}/bin/broadwayd :0 &
        '';

        serviceConfig = {
          User = "nicotine";
          Group = "media";
          ExecStart = "${pkgs.nicotine-plus}/bin/nicotine";
          Restart = "always";
          RestartSec = "5s";
        };
      };

      system.stateVersion = "22.11";
    };
  };
}
