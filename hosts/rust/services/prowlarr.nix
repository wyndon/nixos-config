{
  services.prowlarr = {
    enable = true;
    openFirewall = true;
  };

  services.backup.paths = ["/var/lib/private/prowlarr"];
}
