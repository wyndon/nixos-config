{
  imports = [
    ./jellyfin.nix
    ./lidarr.nix
    ./miniflux.nix
    ./prowlarr.nix
    ./treasure-hunting.nix
  ];
}
