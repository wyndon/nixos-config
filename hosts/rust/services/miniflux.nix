{config, ...}: {
  age.secrets."miniflux/adminCredentials".file = ../../../secrets/miniflux/adminCredentials.age;

  services.miniflux = {
    enable = true;
    adminCredentialsFile = config.age.secrets."miniflux/adminCredentials".path;
    config = {
      BASE_URL = "http://${config.networking.hostName}";
      FETCH_YOUTUBE_WATCH_TIME = "1";
      LISTEN_ADDR = "${config.networking.hostName}";
      PORT = "8080";
    };
  };

  networking.firewall.allowedTCPPorts = [8080];

  services.postgresql-backup.enable = true;
}
