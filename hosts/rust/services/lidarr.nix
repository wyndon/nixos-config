{config, ...}: {
  services.lidarr = {
    enable = true;
    group = "media";
    openFirewall = true;
  };

  users.groups.media.gid = 993;

  services.backup.paths = [config.services.lidarr.dataDir];
}
