{
  services.jellyfin = {
    enable = true;
    group = "media";
    openFirewall = true;
  };

  users.groups.media.gid = 993;

  services.backup.paths = ["/var/lib/jellyfin"];
}
