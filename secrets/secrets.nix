let
  inherit (import ../keys.nix) users hosts;
in {
  "backup/repo-passphrase.age".publicKeys = [
    users.wyndon
    hosts.rust
  ];

  "miniflux/adminCredentials.age".publicKeys = [
    users.wyndon
    hosts.rust
  ];

  "users/root/password-hash.age".publicKeys = [
    users.wyndon
    hosts.rust
  ];

  "users/wyndon/password-hash.age".publicKeys = [
    users.wyndon
    hosts.rust
  ];
}
