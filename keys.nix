{
  hosts = {
    openwrt = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEPtt1i5TmhMO3EtT+8TWHCT3vVI9+mQzFqIzqW+IjFS";
    rust = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHfrdDSv92xrTjhTYtNK685uL8AH5Mm3HDeafLplM/1o";
  };

  users = {
    restic = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIDmM2jv738JD7Dt435FO2zaRWzar0ptXxJA9kygvD8K";
    wyndon = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGDQEoqu63mL3LGVPrcDn2Klinweo07cHEnkNrgw+Ke3";
  };
}
